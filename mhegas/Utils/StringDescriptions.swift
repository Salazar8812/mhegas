//
//  StringDescriptions.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 4/26/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

class StringDescriptions: NSObject {
    static let description_form1 = "Es la cantidad de aire que entra en los pulmones que entrará con cada inspiración programada.\nEl Vt a programar se obtiene en 3 pasos:\n1)    Medir la talla de altura del paciente con cinta métrica.\n2)    Calcular el peso ideal (pacientes sin SDRA) o peso predicho para pacientes con SDRA.\nMultiplicar por los ml deseados (6 a 10 ml x kg de peso ideal para pacientes sin SDRA ó 4 a 8 ml x kg de peso predicho para pacientes con SDRA)."
    
    static let description_form_empty = ""
    
    static let description_form2 = "El peso ideal es una cifra apropiada e individualizada para el cálculo adecuado del volumen corriente (Vt), propuesta por la Organización Mundial de la Salud (OMS) y adaptada a la ventilación mecánica, basada en el IMC normal para cada talla con la finalidad de evitar el daño inducido por Vt excesivo (volutrauma).\nPuede usarse en adultos y niños de todas las tallas, con controversia de su uso para neonatos a término y pretérmino."
    static let description_form3 = "El peso predicho es una cifra propuesta para el cálculo del volumen corriente (Vt), propuesta por grupo ARDSnet, para usarse en pacientes que presntan Síndrome de Distrés Respiratorio Agudo (SDRA) basada en el IMC normal para cada talla con la finalidad de evitar el daño inducido por Vt excesivo (volutrauma). Lo que ha demostrado mejoría en la supervivencia de estos pacientes.\nPuede usarse en adultos que midan más de 152.4 cm."
    static let description_form4 = "La presión positiva al final de la espiración (PEEP) puede ser inicialmente programada de acuerdo al índice de masa corporal del paciente (IMC)."
    static let description_form5 = "La presión sanguínea de dióxido de carbono debe estar ajustada de acuerdo a varios factores, debido a que causa alteraciones del tono vasomotor a nivel pulmonar, cerebral y otros órganos importantes, en paciente pulmonarmente sano la meta será una paCO2 normal para la altura a la que esté habituado el paciente sobre el nivel del mar.\nEl CO2 se modificará con la Frecuencia Respiratoria programada (FR), aumentar la FR generará menor CO2 arterial, disminuir la FR aumentará el CO2 arterial (no aplica para pacientes con atrapamiento aéreo)\nEn pacientes que tengan trastornos metabólicos del estado ácido-base deberá ser ajustado basado en el CO2 (CO2e) esperado ya sea para acidosis metabólica o alcalosis metabólica, se requiere una gasometría arterial para determinar la paCO2 actual (presión arterial parcial de dióxido de carbono).\nPara los pacientes que tengan retención crónica de dióxido de carbono (ej. enfermedad pulmonar obstructiva crónica, síndrome de apnea obstructiva del sueño) deberá ser ajustada de acuerdo a la paCO2 que les permita el mayor equilibrio ácido-base (pH cercano a 7.35)."
    static let description_form6 = "Presión ejercida por la atmósfera a una altura determinada.\n760 mm Hg (a nivel del mar)."
    static let description_form7 = "Presión ejercida por el vapor de agua que ingresa a la vía respiratoria. 47 mm HG."
    static let description_form8 = "Presión de oxígeno arterial a disposición de los tejidos. Normal 85 a 100 mm Hg a nivel del mar. Hasta 60 o 55 mm Hg a mayores alturas."
    static let description_form9 = "Presión de oxígeno que ingresa a la vía respiratoria de conducción."
    static let description_form10 = "Presión de oxígeno que se encuentra dentro de los alveolos."
    static let description_form11 = "Diferencia de presión de oxígeno alveolar al arterial, depende de la capacidad de difusión de la membrana alveolo-capilar."
    static let description_form12 = "Porcentaje de sangre arterial que pasa por la circulación pulmonar sin oxigenarse. Normal menor a 10%."
    static let description_form13 = "Energía total suministrada por el ventilador al tejido pulmonar en una unidad de tiempo.\n\n Menor a 12 J/min se asocia a menor lesión inducida por el ventilador."
    
    static let result_msg_CalculateWeightManOutSDRA = "Multiplique el resultado del peso ideal por 6 a 10 ml para obtener el Vt,\nEjemplo: 8 ml x 70 kg de peso ideal = Vt: 560 ml"
    static let result_msg_CalculateWeightManWithSDRA = "Multiplique el resultado del peso ideal por 4 a 8 ml para obtener el Vt,\nEjemplo: 6 ml x 70 kg de peso ideal = Vt: 420 ml"
    static let result_msg_CalculateWeightManWithSDRA1 = "El rultado se muestra en %\n\nNormal menor a 10%"
    static let result_msg_CalculateFR = "Coloque esta FR para obtener el CO2 deseado, verifique una relación I:E fisiológica (ej. 1:2)."
    static let result_msg_CalculatePresionParcialO2B = "Normal 85 a 100 mm Hg a nivel del mar. Hasta 60 o 55 mm Hg a mayores alturas."
    static let result_msg_CalculateDiferenciaAlveoloArterialO2 = "Normal 10 a 15 mm HG"
    static let result_msg_CalculateRelacionOxigenaciónO2 = "Normal >300"
    static let result_msg_CalculateCortocircuitosIntrapulmonares = "El resultado se muestra en %"
    static let result_msg_PPEP_initial_1 = "Considere PEEP inicial: 5 a 8 cm H2O"
    static let result_msg_PEEP_initial_2 = "Considere PEEP inicial: 10 cm H2O"
    static let result_msg_PEEP_initial_3 = "Considere PEEP inicial: 10 cm H2O"
    
    static let result_msg_1 = "No extubar"
    static let result_msg_2 = "Valorar extubacion"
    static let result_msg_3 = "Más éxito en extubación"
    
    static let result_msg_pmv_1 = "Sugiere protección pulmonar"
    static let result_msg_pmv_2 = "Valorar ajustar parámetros para lograr protección pulmonar (ejem. bajar Vt, bajar Pmeseta, bajar FR)"
    
    static let result_msg_dp_1 = "Se encuentra en meta de protección pulmonar"
    static let result_msg_dp_2 = "Fuera de rango de protección pulmonar"
}
