//
//  ResultValidationsUtils.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 4/29/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

class ResultValidationsUtils: NSObject {
    
    static func validateForPEEPInitial(result : Double) -> String {
        if (result < 30){
        return StringDescriptions.result_msg_PPEP_initial_1
        }else if (result > 29 && result < 41){
        return StringDescriptions.result_msg_PEEP_initial_2
        }else{
            return StringDescriptions.result_msg_PEEP_initial_3
        }
    }
    
    static func validateRRS(result : Double) -> String{
            if (result > 105){
        return StringDescriptions.result_msg_1
            }else if (result > 61 && result < 105){
        return StringDescriptions.result_msg_2
            }else if (result < 60){
        return StringDescriptions.result_msg_3
            }else {
            return ""
        }
    }
    
    static func validatePMV(result: Double)-> String {
            if (result < 12){
        return StringDescriptions.result_msg_pmv_1
            }else if (result > 12){
        return StringDescriptions.result_msg_pmv_2
            }else {
            return ""
        }
    }
    
    static func validateDP(result : Double) -> String{
            if (result < 13){
        return StringDescriptions.result_msg_dp_1
            }else if (result > 13){
        return StringDescriptions.result_msg_dp_2
            }else {
            return "";
        }
    }
}
