//
//  Validator.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 5/2/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

public class Validator: NSObject {
    
    @objc public dynamic var view : UIView!
    
    public func isValid() -> Bool {
        preconditionFailure("This method must be overridden")
    }
    
    public func showError(valid : Bool){
        preconditionFailure("This method must be overridden")
    }
    
}
