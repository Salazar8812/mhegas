//
//  OperationResultViewController.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 4/29/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

class OperationResultViewController: UIViewController {
    var customBlurEffectStyle: UIBlurEffect.Style!
    var customInitialScaleAmmount: CGFloat!
    var customAnimationDuration: TimeInterval!
    
    @IBOutlet weak var mResultValueLabel: UILabel!
    @IBOutlet weak var mDialogContainerView: UIView!
    @IBOutlet weak var mDescriotionLabel: UILabel!
    
    var mResult : String?
    var mFormula : String?
    
    @IBOutlet weak var mEqualImageView: UIImageView!
    override func viewDidLoad() {
        modalPresentationCapturesStatusBarAppearance = true
        mResultValueLabel.text = mResult
        if(mFormula != nil || !mFormula!.isEmpty){
            validateSetRecommendation()
        }
        
        setColorDefault(mImage: mEqualImageView)
    }
    
    func setColorDefault(mImage : UIImageView){
        mImage.image = mImage.image?.withRenderingMode(.alwaysTemplate)
        mImage.tintColor = UIColor(netHex: Colors.color_white)
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return customBlurEffectStyle == .dark ? .lightContent : .default
    }

    @IBAction func mAcceptButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func validateSetRecommendation() {
        switch (mFormula) {
        case "CalculateWeightManOutSDRA":
            mDescriotionLabel.text = StringDescriptions.result_msg_CalculateWeightManOutSDRA
            break;
        case "CalculateWeightManWithSDRA":
            mDescriotionLabel.text = StringDescriptions.result_msg_CalculateWeightManWithSDRA
            break;
        case "CalculatePEEPInitial":
            mDescriotionLabel.text = ResultValidationsUtils.validateForPEEPInitial(result: (mResult?.toDouble())!)
            break;
        case "CalculatePoderMecanicoVentilatorio":
            mDescriotionLabel.text = ResultValidationsUtils.validatePMV(result: mResult!.toDouble()!)
            break;
        case "CalculateDistencionPulmonar":
            mDescriotionLabel.text = ResultValidationsUtils.validateDP(result: mResult!.toDouble()!)
            break;
        case "CalculateAcidosis":
            hiddenRecommendation();
            break;
        case "CalculateRespiracionesRapidasSuperficiales":
            mDescriotionLabel.text = ResultValidationsUtils.validateRRS(result: mResult!.toDouble()!)
        case "CalculateAlcalosis":
            hiddenRecommendation();
            break;
        case "CalculateFR":
            mDescriotionLabel.text = StringDescriptions.result_msg_CalculateFR
            break;
        case "CalculatePresionInspiradaO2":
            hiddenRecommendation();
            break;
        case "CalculatePresionAlveolar":
            hiddenRecommendation();
            break;
        case "CalculatePresionAlveolarO2B":
            hiddenRecommendation();
            break;
        case "CalculateRelacionArterioAlveolarO2":
            hiddenRecommendation();
            break;
        case "CalculateRespiratorioO2":
            hiddenRecommendation();
            break;
        case "CalculatOoxigenación":
            hiddenRecommendation();
            break;
        case "CalculatePresionParcialO2B":
            mDescriotionLabel.text = StringDescriptions.result_msg_CalculatePresionParcialO2B
            break;
        case "CalculateRelacionOxigenaciónO2":
            mDescriotionLabel.text = StringDescriptions.result_msg_CalculateRelacionOxigenaciónO2
            break;
        case "CalculateDiferenciaAlveoloArterialO2":
            mDescriotionLabel.text = StringDescriptions.result_msg_CalculateDiferenciaAlveoloArterialO2
            break;
        case "CalculateCortocircuitosIntrapulmonares":
            mDescriotionLabel.text = StringDescriptions.result_msg_CalculateWeightManWithSDRA1
            break;
        case "CalculateDistensibilidadPulmonarEstatica":
            hiddenRecommendation()
            break;
        case "CalculateDistensibilidadPulmonarDinamica":
            hiddenRecommendation()
            break;
        case "CalculatePresionTransviaAerea":
            hiddenRecommendation()
            break;
        case "CalculateResistenciaViaAerea":
            hiddenRecommendation()
            break;
        default: break
        }
    }
    
    func hiddenRecommendation(){
        
    }
}

extension OperationResultViewController: MIBlurPopupDelegate {
    
    var popupView: UIView {
        return mDialogContainerView ?? UIView()
    }
    
    var blurEffectStyle: UIBlurEffect.Style {
        return .dark
    }
    
    var initialScaleAmmount: CGFloat {
        return 0.51
    }
    
    var animationDuration: TimeInterval {
        return 0.5
    }
    
}

