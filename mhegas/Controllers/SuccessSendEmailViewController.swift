//
//  SuccessSendEmailViewController.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 5/2/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

protocol FinishDelegate: NSObjectProtocol {
    func OnFinish(mViewController : UIViewController)
}


class SuccessSendEmailViewController: UIViewController {

    @IBOutlet weak var mAcceptImageView: UIImageView!
    @IBOutlet weak var mDialogContainerView: UIView!
    @IBOutlet weak var mIconImageView: UIImageView!
    var customBlurEffectStyle: UIBlurEffect.Style!
    var customInitialScaleAmmount: CGFloat!
    var customAnimationDuration: TimeInterval!
    var mFinishDelegate : FinishDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        modalPresentationCapturesStatusBarAppearance = true
        setColorDefault(mImage: mAcceptImageView)
    }
    
    @IBAction func mAcceptButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return customBlurEffectStyle == .dark ? .lightContent : .lightContent
    }
    
    func setColorDefault(mImage : UIImageView){
        mImage.image = mImage.image?.withRenderingMode(.alwaysTemplate)
        mImage.tintColor = UIColor(netHex: Colors.color_white)
    }
}

extension SuccessSendEmailViewController: MIBlurPopupDelegate {
    
    var popupView: UIView {
        return mDialogContainerView ?? UIView()
    }
    
    var blurEffectStyle: UIBlurEffect.Style {
        return .dark
    }
    
    var initialScaleAmmount: CGFloat {
        return 0.51
    }
    
    var animationDuration: TimeInterval {
        return 0.5
    }
    
}
