//
//  ImagesVisorViewController.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 5/1/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

class ImagesVisorViewController: UIViewController, ImageIndicatorDelegate {
    
    @IBOutlet var mDialogContainerView: UIView!
    @IBOutlet weak var mPreviewImageView: UIImageView!
    @IBOutlet weak var mCarouselPhotoiCarousel: iCarousel!
    var mImagePreviewCarousel : VisorImagesDataSource!
    var mListImage : [String] = []
    
    var customBlurEffectStyle: UIBlurEffect.Style!
    var customInitialScaleAmmount: CGFloat!
    var customAnimationDuration: TimeInterval!
    var mPhotoCLicked : String?

    override func viewDidLoad() {
        super.viewDidLoad()
        modalPresentationCapturesStatusBarAppearance = true
        mImagePreviewCarousel = VisorImagesDataSource(carrusel: mCarouselPhotoiCarousel,mImageIndicatorDelegate: self)
        mImagePreviewCarousel.settings()
        mPreviewImageView.image = UIImage(named: mPhotoCLicked!)
        populateImages()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return customBlurEffectStyle == .dark ? .lightContent : .lightContent
    }
    
    func populateImages(){
        mListImage.append("ic_photo1")
        mListImage.append("ic_photo2")
        mListImage.append("ic_photo3")
        mListImage.append("ic_photo4")
        mListImage.append("ic_photo5")
        mListImage.append("ic_photo6")
        mListImage.append("ic_photo7")
        mListImage.append("ic_photo8")
        mListImage.append("ic_photo9")
        mImagePreviewCarousel.update(items: mListImage)
        mCarouselPhotoiCarousel.scrollToItem(at: 3, animated: true)

    }

    func OnSelectedImage(mItemSelected: String) {
        mPreviewImageView.image = UIImage(named: mItemSelected)
    }
    @IBAction func mCloseButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension ImagesVisorViewController: MIBlurPopupDelegate {
    
    var popupView: UIView {
        return mDialogContainerView ?? UIView()
    }
    
    var blurEffectStyle: UIBlurEffect.Style {
        return .dark
    }
    
    var initialScaleAmmount: CGFloat {
        return 0.51
    }
    
    var animationDuration: TimeInterval {
        return 0.5
    }
    
}

