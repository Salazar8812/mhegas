//
//  CommentsViewController.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 4/10/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

class CommentsViewController: UIViewController, TableViewCellClickDelegate {
    
    @IBOutlet weak var mSearchImageView: UIImageView!
    @IBOutlet weak var mListConceptTableView: UITableView!
    @IBOutlet weak var mSearchTextField: UITextField!
    var mDataSource : BaseDataSource<NSObject, ItemConceptTableViewCell>?
    var mListFomulas : [Formulas] = []
    var mListAux : [Formulas] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        populateFormulas()
        mDataSource = BaseDataSource(tableView: self.mListConceptTableView, delegate: self)
        mDataSource?.setHeightRow(height:65)
        mDataSource?.update(items: mListFomulas)
        
        addTarget()
        setTintcolor()
    }
    
    func setTintcolor(){
        mSearchImageView.image = mSearchImageView.image?.withRenderingMode(.alwaysTemplate)
        //mSearchImageView.tintColor = UIColor(netHex: Colors.color_background_app)
    }
    
    func addTarget(){
        mSearchTextField.addTarget(self, action: #selector(watcher(_:)), for: .editingChanged)
    }
    
    @objc func watcher(_ textfield : UITextField){
        var cadAux : String = ""
        cadAux = textfield.text!
        mListAux.removeAll()
        if(cadAux.count == 0 || cadAux == ""){
            mDataSource!.update(items: mListFomulas)
        }else{
            for item in mListFomulas{
                if(item.mTipoFormula.uppercased().contains(cadAux.uppercased())){
                    mListAux.append(item)
                }
            }
            mDataSource!.update(items: mListAux)
        }
    }
    
    func populateFormulas(){
        mListFomulas.append(Formulas(mTipoFormula: "Peso ideal", mNombreFormula: "Peso ideal (Pacientes sin SDRA)", mIdFormula: "1", mParams: ["Talla (cm)2 *"],mFunctionCalculate: "CalculateWeightManOutSDRA", mDescription: StringDescriptions.description_form2));
        mListFomulas.append(Formulas(mTipoFormula: "Peso predicho para SDRA (ARDSnet)", mNombreFormula: "Peso predicho para SDRA (ARDSnet)", mIdFormula: "1",mParams: ["Talla (cm) *"],mFunctionCalculate: "CalculateWeightManWithSDRA",mDescription: StringDescriptions.description_form3));
        mListFomulas.append(Formulas(mTipoFormula: "PEEP inicial", mNombreFormula: "IMC", mIdFormula: "1",mParams: ["Peso (Kg) *", "(m)2 *"],mFunctionCalculate: "CalculatePEEPInitial",mDescription: StringDescriptions.description_form4));
        
        //nuevas
        
        mListFomulas.append(Formulas(mTipoFormula: "Corrección de CO2", mNombreFormula: "CO2e para Acidosis Metabólica", mIdFormula: "1",mParams: ["Vt (ml) *","Pmeseta *","PEEP *"],mFunctionCalculate: "CalculateDistensibilidadPulmonarEstatica", mDescription: StringDescriptions.description_form5));
        mListFomulas.append(Formulas(mTipoFormula: "Presión Barométrica (PB)", mNombreFormula: "Distensibilidad pulmonar dinámica (Cdyn compliance dinámica)", mIdFormula: "1",mParams: ["Vt (ml) *","Pmax *","PEEP *"],mFunctionCalculate: "CalculateDistensibilidadPulmonarDinamica", mDescription: StringDescriptions.description_form6));
        mListFomulas.append(Formulas(mTipoFormula: "Presión de Vapor de Agua [P H2O]", mNombreFormula: "Presión de Vapor de Agua [P H2O]a", mIdFormula: "1",mParams: ["Pmax *","Pmeseta *"],mFunctionCalculate: "CalculatePresionTransviaAerea", mDescription: StringDescriptions.description_form7));
        mListFomulas.append(Formulas(mTipoFormula: "Presión parcial de O2 Arterial (PaO2)", mNombreFormula: "Presión parcial de O2 Arterial (PaO2)", mIdFormula: "1",mParams: ["Pmax *","Pmeseta *", "Flujo *"],mFunctionCalculate: "CalculateResistenciaViaAerea", mDescription: StringDescriptions.description_form8));
        
        mListFomulas.append(Formulas(mTipoFormula: "Presión Inspirada de O2 (PIO2)", mNombreFormula: "Presión Inspirada de O2 (PIO2)", mIdFormula: "1",mParams: ["HCO3 *"],mFunctionCalculate: "CalculateAcidosis",mDescription: StringDescriptions.description_form9));
        mListFomulas.append(Formulas(mTipoFormula: "Presión Alveolar de O2 (PAO2)", mNombreFormula: "Presión Alveolar de O2 (PAO2)", mIdFormula: "1",mParams: ["HCO3 *"],mFunctionCalculate: "CalculateAlcalosis",mDescription: StringDescriptions.description_form10));
        mListFomulas.append(Formulas(mTipoFormula: "Diferencia Alveolo-arterial de O2 [P (A-a)O2]", mNombreFormula: "Diferencia Alveolo-arterial de O2 [P (A-a)O2]", mIdFormula: "1",mParams: ["Fr *","paCO2 *","CO2e *"],mFunctionCalculate: "CalculateFR", mDescription: StringDescriptions.description_form11));
        mListFomulas.append(Formulas(mTipoFormula: "Cortocircuitos Intrapulmonares (Qs / Qt)", mNombreFormula: "Cortocircuitos Intrapulmonares (Qs / Qt)", mIdFormula: "1",mParams: ["Presión meseta *","PEEP *"],mFunctionCalculate: "CalculateDistencionPulmonar", mDescription: StringDescriptions.description_form12));
        mListFomulas.append(Formulas(mTipoFormula: "Poder mecánico", mNombreFormula: "Poder mecánico", mIdFormula: "1",mParams: ["Driving pressure *","Pmax *", "FR *" , "VT (lt) *"],mFunctionCalculate: "CalculatePoderMecanicoVentilatorio", mDescription: StringDescriptions.description_form13));
        //listaUsuarios.add(new Formulas("Taller gasométrico respiratorio", "Presión parcial de O2 Arterial (PaO2)", "1",new String[]{"Edad en años *"},"CalculatePresionParcialO2A", getString(R.string.description_form_empty)));
       
        
    }
    
    func onTableViewCellClick(item: NSObject, cell: UITableViewCell) {
        let mFormula = item as! Formulas
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle(for: OperationResultViewController.self))
        
        let v1 = storyboard.instantiateViewController(withIdentifier: "DescriptionFormulasViewController") as! DescriptionFormulasViewController
        v1.mTitle = mFormula.mTipoFormula
        v1.mContentDescription = mFormula.mDescription
        MIBlurPopup.show(v1, on: self)
    }

}
