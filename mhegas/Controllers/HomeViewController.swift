//
//  HomeViewController.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 4/10/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func mPhoto1Button(_ sender: Any) {
        launchPreview(mPhotoCLicked: "ic_photo1")
    }
    
    @IBAction func mPhotoButton2(_ sender: Any) {
        launchPreview(mPhotoCLicked: "ic_photo2")
    }
    
    @IBAction func mPhoto3Button(_ sender: Any) {
        launchPreview(mPhotoCLicked: "ic_photo3")
    }
    
    @IBAction func mPhoto4Button(_ sender: Any) {
        launchPreview(mPhotoCLicked: "ic_photo4")
    }
    
    @IBAction func mPhoto5Button(_ sender: Any) {
        launchPreview(mPhotoCLicked: "ic_photo5")
    }
    
    @IBAction func mPhoto6Button(_ sender: Any) {
        launchPreview(mPhotoCLicked: "ic_photo6")
    }
    
    @IBAction func mPhoto7Button(_ sender: Any) {
        launchPreview(mPhotoCLicked: "ic_photo7")
    }
    
    @IBAction func mPhoto8Button(_ sender: Any) {
        launchPreview(mPhotoCLicked: "ic_photo8")
    }
  
    
    @IBAction func mPhoto9Button(_ sender: Any) {
        launchPreview(mPhotoCLicked: "ic_photo9")
    }
    
    func launchPreview(mPhotoCLicked: String){
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle(for: OperationResultViewController.self))
        
        let v1 = storyboard.instantiateViewController(withIdentifier: "ImagesVisorViewController") as! ImagesVisorViewController
       v1.mPhotoCLicked = mPhotoCLicked
        MIBlurPopup.show(v1, on: self)
    }
    
}
