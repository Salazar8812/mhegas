//
//  ViewController.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 4/10/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var mContainer: UIView!
    @IBOutlet weak var mHomeIconImageView: UIImageView!
    @IBOutlet weak var mFormulaImageView: UIImageView!
    
    @IBOutlet weak var mContactImageView: UIImageView!
    @IBOutlet weak var mConceptImageView: UIImageView!
    @IBOutlet weak var mHomeLabel: UILabel!
    @IBOutlet weak var mFormulasLabel: UILabel!
    @IBOutlet weak var mContactLabel: UILabel!
    @IBOutlet weak var mConceptLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        setHome()
        setColorSelected(mImage: mHomeIconImageView, mTitle: mHomeLabel)
        setColorDefault(mImage: mFormulaImageView, mTitle: mFormulasLabel)
        setColorDefault(mImage: mConceptImageView, mTitle: mConceptLabel)
        setColorDefault(mImage: mContactImageView, mTitle: mContactLabel)
    }
    
    func setHome(){
        let news = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        news.view.frame = mContainer.bounds
        mContainer.addSubview(news.view)
        addChildViewController(news)
        news.didMove(toParentViewController: self)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func mHomeButton(_ sender: Any) {
        let news = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        news.view.frame = mContainer.bounds
        mContainer.addSubview(news.view)
        addChildViewController(news)
        news.didMove(toParentViewController: self)
        
        setColorSelected(mImage: mHomeIconImageView, mTitle: mHomeLabel)
        setColorDefault(mImage: mFormulaImageView, mTitle: mFormulasLabel)
        setColorDefault(mImage: mConceptImageView, mTitle: mConceptLabel)
        setColorDefault(mImage: mContactImageView, mTitle: mContactLabel)
    }
    
    @IBAction func mFormulaButton(_ sender: Any) {
        let news = self.storyboard?.instantiateViewController(withIdentifier: "FormulasViewController") as! FormulasViewController
        news.view.frame = mContainer.bounds
        mContainer.addSubview(news.view)
        addChildViewController(news)
        news.didMove(toParentViewController: self)
        setColorDefault(mImage: mHomeIconImageView, mTitle: mHomeLabel)
        setColorSelected(mImage: mFormulaImageView, mTitle: mFormulasLabel)
        setColorDefault(mImage: mConceptImageView, mTitle: mConceptLabel)
        setColorDefault(mImage: mContactImageView, mTitle: mContactLabel)
    }
    
    @IBAction func mConceptButton(_ sender: Any) {
        let news = self.storyboard?.instantiateViewController(withIdentifier: "CommentsViewController") as! CommentsViewController
        news.view.frame = mContainer.bounds
        mContainer.addSubview(news.view)
        addChildViewController(news)
        news.didMove(toParentViewController: self)
        setColorDefault(mImage: mHomeIconImageView, mTitle: mHomeLabel)
        setColorDefault(mImage: mFormulaImageView, mTitle: mFormulasLabel)
        setColorSelected(mImage: mConceptImageView, mTitle: mConceptLabel)
        setColorDefault(mImage: mContactImageView, mTitle: mContactLabel)
    }
    
    @IBAction func mContactButton(_ sender: Any) {
        let news = self.storyboard?.instantiateViewController(withIdentifier: "ContactViewController") as! ContactViewController
        news.view.frame = mContainer.bounds
        mContainer.addSubview(news.view)
        addChildViewController(news)
        news.didMove(toParentViewController: self)
        setColorDefault(mImage: mHomeIconImageView, mTitle: mHomeLabel)
        setColorDefault(mImage: mFormulaImageView, mTitle: mFormulasLabel)
        setColorDefault(mImage: mConceptImageView, mTitle: mConceptLabel)
        setColorSelected(mImage: mContactImageView, mTitle: mContactLabel)
    }
    
    func setColorDefault(mImage : UIImageView, mTitle: UILabel){
        mTitle.textColor = UIColor(netHex: Colors.color_blue)
        mImage.image = mImage.image?.withRenderingMode(.alwaysTemplate)
        mImage.tintColor = UIColor(netHex: Colors.color_blue)
    }
    
    func setColorSelected(mImage : UIImageView, mTitle: UILabel){
        mTitle.textColor = UIColor(netHex: Colors.color_yellow)
        mImage.image = mImage.image?.withRenderingMode(.alwaysTemplate)
        mImage.tintColor = UIColor(netHex: Colors.color_yellow)
    }
}

