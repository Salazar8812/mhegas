//
//  Formulas.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 4/11/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

class Formulas: NSObject {
    var mTipoFormula : String = ""
    var mNombreFormula : String = ""
    var mIdFormula : String = ""
    var mParams : [String] = []
    var mFunctionCalculate : String = ""
    var mDescription: String = ""
 
    init(mTipoFormula : String, mNombreFormula : String, mIdFormula: String, mParams : [String], mFunctionCalculate : String, mDescription : String ) {
        self.mTipoFormula = mTipoFormula
        self.mNombreFormula = mNombreFormula
        self.mIdFormula = mIdFormula
        self.mParams = mParams
        self.mFunctionCalculate = mFunctionCalculate
        self.mDescription = mDescription
    }
    
    
}
