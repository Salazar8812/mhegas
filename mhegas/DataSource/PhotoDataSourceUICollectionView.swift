//
//  PhotoDataSourceUICollectionView.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 4/25/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

class PhotoDataSourceUICollectionView: NSObject, UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    let reuseIdentifier = "PhotoCellReusableViewCollectionViewCell" // also enter this string as the cell identifier in the storyboard
    var mListPhotos : [String] = []
    var mListPhotosCollectionView : UICollectionView!
    
    init(mListPhotos : [String], mListPhotosCollectionView : UICollectionView) {
        super.init()
        self.mListPhotos = mListPhotos
        self.mListPhotosCollectionView = mListPhotosCollectionView
        self.mListPhotosCollectionView.delegate = self
        self.mListPhotosCollectionView.dataSource = self
    }
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.mListPhotos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 110.0, height: 150.0)
    }

    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! PhotoCellReusableViewCollectionViewCell
        
        cell.mImage.image = UIImage(named : mListPhotos[indexPath.item])
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
    }

}
