//
//  ItemConceptTableViewCell.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 4/29/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

class ItemConceptTableViewCell: BaseTableViewCell {
    @IBOutlet weak var mConceptLabel: UILabel!
    
    @IBOutlet weak var mArrowImageView: UIImageView!
    @IBOutlet weak var mIconBookImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func pupulate(object: NSObject) {
        let mFormula = object as! Formulas
        mConceptLabel.text = mFormula.mTipoFormula
        setColorDefault(mImage: mArrowImageView)
        setColorDefault(mImage: mIconBookImageView)
    }
    
    func setColorDefault(mImage : UIImageView){
        mImage.image = mImage.image?.withRenderingMode(.alwaysTemplate)
        mImage.tintColor = UIColor(netHex: Colors.color_yellow)
    }
    
    override public func toString() -> String{
        return "ItemConceptTableViewCell"
    }
}
