//
//  ItemFormulaBaseTableViewCell.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 4/11/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

class ItemFormulaBaseTableViewCell: BaseTableViewCell {
    @IBOutlet weak var mDescriptionFormulaLabel: UILabel!
    @IBOutlet weak var mTypeFormulaLabel: UILabel!
    
    @IBOutlet weak var mIconFormulaImageView: UIImageView!
    @IBOutlet weak var mArrowImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func pupulate(object: NSObject) {
        let mFormula = object as! Formulas
        setColorDefault(mImage: mIconFormulaImageView)
        setColorDefault(mImage: mArrowImageView)
        mDescriptionFormulaLabel.text = mFormula.mNombreFormula
        mTypeFormulaLabel.text = mFormula.mTipoFormula
    }
    
    func setColorDefault(mImage : UIImageView){
        mImage.image = mImage.image?.withRenderingMode(.alwaysTemplate)
        mImage.tintColor = UIColor(netHex: Colors.color_yellow)
    }
    
    override public func toString() -> String{
        return "ItemFormulaBaseTableViewCell"
    }
}
